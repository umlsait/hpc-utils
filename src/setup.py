#!/usr/bin/python

from setuptools import setup

setup(
    name = 'hpc-utils',
    version = '1.0',
    description = 'Utilities for high performance computing clusters at the University of Michigan',
    author = 'Mark Montague',
    author_email = 'hpc-support@umich.edu',
    license = 'GPL3',
    url = 'https://bitbucket.org/umlsait/hpc-utils',
    install_requires = [ 'python-daemon' ],
    packages = [ 'torque' ],
    scripts = [ 'freenodes', 'freealloc', 'lsa_flux_check', 'cancel-my-jobs', 'idlenodes', 'maxwalltime' ],
    )

