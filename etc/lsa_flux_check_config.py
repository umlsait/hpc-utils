
# For the following accounts, allow the specified users to use
# the LSA public allocations even if they are on the userlist for
# these accounts.

ignore_accounts = {
     'biophysics_flux': [ 'meiners' ],
     'lehnertn_flux': [ 'shanajam' ],
     'engin_flux': [ 'zrwang' ],
     'scjp_ada_fluxod': [ 'ykyono' ],
     'scjp_fluxod': [ 'ykyono' ],
     'earth523w17_fluxm': [ 'sgrim' ],
     'support_flux': [ 'crstock' ],
     'support_fluxm': [ 'crstock' ],
     'support_fluxg': [ 'crstock' ],
}

