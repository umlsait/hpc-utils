ABOUT
=====

`hpc-utils` -- Miscellaneous utlities for use on ARC-TS HPC clusters at the University of Michigan


SYNOPSIS
========

`freealloc` - displays unused cores and memory for an allocation

    freealloc [-h] [--jobs] allocation_name


`lsa_flux_check` - checks to see if a user is complying with the usage limits for the LSA public Flux allocaitons

    lsa_flux_check [-h] [--all] [--details] [--mail] [--daemonize] [--submit]

    optional arguments:
      -h, --help   show this help message and exit
      --all        check the usage of all users
      --details    display extra information
      --mail       send mail to offending users (Flux support staff only)
      --daemonize  run as a daemon (Flux support staff only)
      --submit     run as a self-resubmitting job (Flux support staff only)


`idlenodes` - display idle whole nodes with a give property/feature

    idlenodes [-h] [-a] account_name [property_name]

    positional arguments:
      account_name   display nodes usable by this account/allocation
      property_name  (optional) display only nodes having this property

    optional arguments:
      -h, --help     show this help message and exit
      -a, --all      display all nodes, not just idle ones

    example: show idle Haswell (24 core) nodes usable by jobs submitted to lsa_flux:
      idlenodes lsa_flux haswell



INSTALLATION
============

`hpc-utils` requires:

* [TORQUE](http://www.adaptivecomputing.com/products/open-source/torque/)
* [python-daemon](https://pypi.python.org/pypi/python-daemon)
* argparse (included with Python 2.7 and later, and with Python 3.2 and later)

The following commands are very rough and are specific to Flux:

```bash
exec newgrp lsaswadm
umask 0002

sudo mkdir /usr/local/hpc-utils
sudo chown ${USER}:lsaswadm /usr/local/hpc-utils
sudo chmod 2775 /usr/local/hpc-utils

git clone https://bitbucket.org/umlsait/hpc-utils
cd hpc-utils/src

export INSTALL_DIR=/sw/arcts/centos7/hpc-utils

python ./setup.py build 2>&1 | tee log.build

# We install everything into the repo and the ARC-TS systems group will
# clone the entire repo (installed software plus source) as a part of
# their deployment workflow.

# Use "--single-version-externally-managed --record /dev/null" to force
# setuptools to not wrap the Python scripts.  Wrapping the scripts fails
# since hpc-utils modifies its own PYTHONPATH (the wrappers don't know
# where to look to find the actual scripts).

# Install the dependencies:
PYTHONPATH=${INSTALL_DIR}/lib/python2.7/site-packages/ \
  python ./setup.py install --prefix $(/usr/bin/pwd)/.. \
  2>&1 | tee log.install
rm ../bin/{freenodes,freealloc,lsa_flux_check,cancel-my-jobs,idlenodes}
rm -f ../bin/easy-install*
# Do the install for real:
PYTHONPATH=${INSTALL_DIR}/lib/python2.7/site-packages/ \
  python ./setup.py install --prefix $(/usr/bin/pwd)/.. \
  --single-version-externally-managed --record /dev/null \
  2>&1 | tee -a log.install

# Create a symlink for the original name:
( cd ${INSTALL_DIR}/bin ; ln -s maxwalltime tto  )

# Install qpeek:
cp qpeek ${INSTALL_DIR}/bin/

# .../hpc-utils/bin is in users' default PATH, but /usr/arc-connect is not
# so create a symlink to make the VNC and Jupyter scripts available:
( cd ${INSTALL_DIR}/bin ;
  for s in /usr/arc-connect/flux/centos7/bin/{jupyter,vnc}-* ; do
    ln -s $s
  done )


mkdir socks
cd socks
# Download the Dante source code from http://www.inet.no/dante/download.html
wget http://www.inet.no/dante/files/dante-1.4.2.tar.gz
tar zxf dante-1.4.2.tar.gz
cd dante-1.4.2
./configure --prefix=${INSTALL_DIR} \
  --with-socks-conf=/sw/arcts/centos7/hpc-utils/etc/socks.conf \
  --with-sockd-conf=/sw/arcts/centos7/hpc-utils/etc/sockd.conf \
  2>&1 | tee log.socks.configure
make 2>&1 | tee log.socks.make
make install 2>&1 | tee log.socks.install
perl -p -i -e "s%${INSTALL_DIR}%/sw/arcts/centos7/hpc-utils%g;" \
  ${INSTALL_DIR}/bin/socksify
rm -rf ${INSTALL_DIR}/sbin
mkdir -p ${INSTALL_DIR}/etc
cat > ${INSTALL_DIR}/etc/socks.conf <<__EOF__
resolveprotocol: tcp  # work around ssh proxying only TCP, not needed if using a real SOCKS server
route {
        from: 0.0.0.0/0 to: 0.0.0.0/0 via: 127.0.0.1 port = 1080
        proxyprotocol: socks_v4 socks_v5
        method: none
}
__EOF__


# Set permissions so that lsaswadm can administer:
chmod -R g+rwX,o+rX ${INSTALL_DIR}
find ${INSTALL_DIR} -type d | xargs chmod g+s
sudo chown -R root:lsaswadm ${INSTALL_DIR}

```


SUPPORT
=======

Please send any questions, feedback, requests, or patches to hpc-support@umich.edu


LICENSE
=======

`hpc-utils` is Copyright (C) 2013 - 2017 Regents of The University of Michigan.

`hpc-utils` is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

`hpc-utils` is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with `hpc-utils`.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/)

